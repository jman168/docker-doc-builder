#!/bin/sh

echo "Welcome to the docker doc builder!"

cd docs

if test -f "requirements.txt"; then
    echo "Requirements file found, installing..."
    pip3 install -r requirements.txt
    echo "Done."
fi

if test -f "conf.py"; then
    echo "Sphinx documentation detected, proceding with sphinx build..."
    make html -e "BUILDDIR=_build"
    chmod -R 777 _build
    echo "Done."
fi

if test -f "mkdocs.yml"; then
    echo "mkdocs documentation detected, proceeding with mkdocs build..."
    mkdocs build -d _build/html
    chmod -R 777 _build
    echo "Done."
fi