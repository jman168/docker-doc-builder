# Docker Doc Builder

Docker Doc Builder is a simple docker container for building sphinx and mkdoc documentation.

## Usage

```docker run --rm -v [/path/to/documentation/folder/]:/docs jman168/docker-doc-builder```

This will result in a _build folder being created in the root of the documentation folder. In the _build folder will be folder called html which contains the static html files to be hosted.