FROM alpine:latest

COPY build.sh .

RUN apk update && apk upgrade && \
    apk add python3 python3-dev py3-pip musl-dev make gcc && \
    pip3 install sphinx mkdocs

CMD ["./build.sh"]